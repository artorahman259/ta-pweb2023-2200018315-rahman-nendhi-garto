<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rahman Nendhiarto</title>
    <link rel="stylesheet" href="portofolio.css">  
    <script>
        function sendMessage() {
            var message = document.getElementById("messageInput").value;
            alert("Pesan terkirim: " + message);
        }
    </script>
</head>
<body>
    <header>
        <nav>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="#">Portfolio</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </nav>
        <section>
            <div class="container">
                <div class="profile">
                    <img src="haha.jpeg" alt="">
                    <h2>Rahman Nendhiarto</h2>
                    <h4>Web Developer</h4>
                </div>
            </div>
        </section>
    </header>

    <main>
        <div class="container">
            <div class="about">
                <h2>Media Sosial</h2>
                <div medsos="grid-container">
                    <img src=ig.jpeg width = 80px>@astronotmaman</img>
                    <br>
                    <br>
                    <img src=twitter.png width = 80px>@astronotmaman</img>
                    <br>
                    <br>
                    <img src=fb.png width = 80px>astronotmaman</img>
                    <br>
                    <br>
                    <img src=line.png width = 80px>0822-4115-9116</img>
                    <br>
                    <br>
                    <img src=wa.jpeg width = 80px>0822-4115-9116</img>
                    <br>
                    <br>
                </div>
            </div>
            <div class="alamat">
                <h2>Alamat</h2>
                <br>
                <ul>
                    <p1>Tamanan, Kec. Banguntapan, Kabupaten Bantul, Daerah Istimewa Yogyakarta 55191</p1>
                    <br>
                    <br>
                </ul>
            </div>
            <div class="organitation">
                
            </div>
        </div>
    </main>

    <footer>
        <div class="container">
            <div class="contact">
                <h4>Kontak</h4>
                <p>Email: artorahman259@gmail.com</p>
                <p>Phone: 123-456-7890</p>
                <input type="text" id="messageInput" placeholder="Masukkan pesan">
                <button onclick="sendMessage()">Kirim Pesan</button>
            </div>
        </div>
    </footer>
</body>
</html>

<?php
    if(isset($_POST['submit'])) {
        $message = $_POST['messageInput'];
        echo "<script>alert('Pesan terkirim: " . $message . "');</script>";
    }
?>
