<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rahman Nendhiarto</title>
    <link rel="stylesheet" href="portofolio.css">  
    <script>
        function sendMessage() {
            var message = document.getElementById("messageInput").value;
            alert("Pesan terkirim: " + message);
        }
    </script>
</head>
<body>
    <header>
        <nav>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="#about.php">About</a></li>
                <li><a href="#">Portfolio</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </nav>
        <section>
            <div class="container">
                <div class="profile">
                    <img src="haha.jpeg" alt="">
                    <h2>Rahman Nendhiarto</h2>
                    <h4>Web Developer</h4>
                </div>
            </div>
        </section>
    </header>

    <main>
        <div class="container">
            <div class="about">
                <h2>Tentang Saya</h2>
                <div class="grid-container">
                    <div class="grid-item">
                        <p>Sebagai seorang mahasiswa jurusan Informatika, saya memiliki minat yang kuat dalam teknologi dan pengembangan perangkat lunak. Saya tertarik untuk mempelajari berbagai aspek informatika, termasuk pemrograman, analisis data, kecerdasan buatan, dan pengembangan aplikasi. Saya percaya bahwa teknologi memiliki potensi besar untuk mengubah dunia dan saya bersemangat untuk menjadi bagian dari perubahan tersebut.</p>
                    </div>
                    <div class="grid-item">
                        <p>Selain itu, saya aktif terlibat dalam proyek-proyek kelompok dan komunitas mahasiswa, di mana saya dapat berkolaborasi dengan teman sejurusan untuk mengembangkan proyek-proyek yang inovatif dan bermanfaat. Saya juga berpartisipasi dalam kompetisi-kompetisi teknologi untuk menguji kemampuan dan kreativitas saya.</p>
                    </div>
                    <div class="grid-item">
                        <p>Dengan semangat dan dedikasi saya, saya siap menghadapi tantangan dalam dunia informatika dan berkontribusi dalam menciptakan solusi teknologi yang inovatif dan relevan bagi masyarakat.</p>
                    </div>
                </div>
            </div>
            <div class="education">
                <h2>Riwayat Pendidikan</h2>
                <br>
                <ul>
                    <li>SD Negeri 2 baseh (2014)</li>
                    <li>SMP Modern purwokerto (2018)</li>
                    <li>SMA Negeri 1 Banyumas (2021)</li>
                    <li>Universitas Ahmad Dahlan Yogyakarta - Informatika (2022)</li>
                    <br>
                </ul>
            </div>
            <div class="organitation">
                <h2>Riwayat Organisasi & Kepanitiaan</h2>
                <br>
                <ul>
                    <li>Panitia Acara 17an Tingkat Desa (2022)</li>
                    <li>-</li>
                    <li>-</li>
                    <li>-</li>
                </ul>
            </div>
        </div>
    </main>

    <footer>
    </footer>
</body>
</html>


